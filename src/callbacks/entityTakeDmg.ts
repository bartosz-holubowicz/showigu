import { CollectibleTypeCustom } from "../constants";
import { entityDistanceBetween, entityVectorBetween } from "../utils/entities";
import { setVectorLength, vectorLength } from "../utils/math";
import { playerHasItem } from "../utils/player";

export function main(
  tookDamage: Entity,
  _damageAmount: number,
  _damageFlags: DamageFlag,
  damageSource: EntityRef,
  _damageCountdownFrames: number,
): boolean | null {
  const hasChewingGum = playerHasItem(
    CollectibleTypeCustom.COLLECTIBLE_CHEWING_GUM,
  );

  if (hasChewingGum && tookDamage.IsVulnerableEnemy()) {
    const shouldBounce: boolean =
      !damageSource.IsFriendly && damageSource.Type === EntityType.ENTITY_TEAR;

    if (!shouldBounce) {
      return false;
    }

    const roomEnemies = Isaac.GetRoomEntities().filter((e: Entity) =>
      e.IsVulnerableEnemy(),
    );

    if (roomEnemies.length <= 1) {
      return null;
    }

    // TODO: Mark hit enemies to not be targeted later by bouncing tear
    const closestEnemy = Isaac.GetRoomEntities()
      .filter(
        (e: Entity) => e.IsVulnerableEnemy() && e.Index !== tookDamage.Index,
      )
      .map((e: Entity) => ({
        entity: e,
        distance: entityDistanceBetween(tookDamage, e),
      }))
      .sort((obj1, obj2) => obj1.distance - obj2.distance)[0];

    // Rotate tear vector towards the closest enemy
    const newTearVelocity = setVectorLength(
      entityVectorBetween(tookDamage, closestEnemy.entity),
      vectorLength(damageSource.Entity.Velocity),
    );

    damageSource.Entity.Velocity = newTearVelocity;

    return true;
  }

  return null;
}
