import { CollectibleTypeCustom } from "../constants";
import { playerHasItem } from "../utils/player";

export function checkApplyEffect(): void {
  playerHasItem(CollectibleTypeCustom.COLLECTIBLE_CHEWING_GUM, applyEffect);
}

function applyEffect(player: EntityPlayer) {
  player.TearFlags |= TearFlags.TEAR_PIERCING;
}
