import * as entityTakeDmg from "./callbacks/entityTakeDmg";
import * as postUpdate from "./callbacks/postUpdate";

// Register the mod
// (which will make it show up in the list of mods on the mod screen in the main menu)
const chewingGum = RegisterMod("Chewing gum", 1);

// Register callbacks
chewingGum.AddCallback(ModCallbacks.MC_POST_UPDATE, postUpdate.main);
chewingGum.AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, entityTakeDmg.main);
