import { calculateDistance } from "./math";

export function entityDistanceBetween(
  entity1: Entity,
  entity2: Entity,
): number {
  return calculateDistance(entity1.Position, entity2.Position);
}

export function entityVectorBetween(entity1: Entity, entity2: Entity): Vector {
  return entity2.Position.sub(entity1.Position);
}
