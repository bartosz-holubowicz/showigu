export function calculateDistance(pos1: Vector, pos2: Vector): number {
  return ((pos2.X - pos1.X) ** 2 + (pos2.Y - pos1.Y) ** 2) ** 0.5;
}

export function vectorLength(v: Vector): number {
  return (v.X ** 2 + v.Y ** 2) ** 0.5;
}

export function rotateVector(v: Vector, angle: number): Vector {
  const { sin, cos } = Math;

  return Vector(
    cos(angle) * v.X - sin(angle) * v.Y,
    sin(angle) * v.X + cos(angle) * v.Y,
  );
}

export function normalizeVector(v: Vector): Vector {
  const length = (v.X ** 2 + v.Y ** 2) ** 0.5;

  return Vector(v.X / length, v.Y / length);
}

export function setVectorLength(v: Vector, newLength: number): Vector {
  const desiredLength = newLength / vectorLength(v);

  return Vector(v.X * desiredLength, v.Y * desiredLength);
}
