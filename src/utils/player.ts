export function playerHasItem(
  itemId: number,
  callback: (p: EntityPlayer) => void = () => {},
): boolean {
  const game = Game();
  const numPlayers = game.GetNumPlayers();

  let someoneHasTheItem = false;

  for (let i = 1; i <= numPlayers; i++) {
    const player = Isaac.GetPlayer(i);

    if (player !== null && player.HasCollectible(itemId)) {
      callback(player);
      someoneHasTheItem = true;
    }
  }

  return someoneHasTheItem;
}
